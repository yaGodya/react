import React from "react";
import "./CardContent.scss";

export default function CardContent({ card, isFavorite, toggleFavorite }) {
    return (
        <div>
            <img
                src={card.imagePath}
                alt={card.name}
                style={{ width: "300px", height: "300px" }}
            />
            <div className="card-item-content">
                <div className="card-item_box">
                    <h2 className="card-item_title">{card.name}</h2>
                    <span
                        className="card-item_star"
                        onClick={() => toggleFavorite(card)}
                    >
                        {isFavorite ? "★" : "☆"}
                    </span>
                </div>
                <p>Цена: {card.price} грн.</p>
                <p>Артикул: {card.article}</p>
                <div className="color-item">Цвет:{card.color}</div>
            </div>
        </div>
    );
}
