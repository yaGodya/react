import React from "react";
import "./Pages.scss";
import ProductList from "../ProductsList";

function Cart({ favorites, cart, toggleFavorite, isInCart, onClick }) {
    const isCartPage = true;
    return (
        <div className="main__page">
            <ProductList
                toggleFavorite={toggleFavorite}
                cards={cart}
                favorites={favorites}
                isInCart={isInCart}
                onClick={onClick}
                cart={cart}
                isCartPage={isCartPage}
            />
        </div>
    );
}

export default Cart;
