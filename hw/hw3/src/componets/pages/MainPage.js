import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import ProductList from "../ProductsList";
import getData from "../ApiServer";
import "./Pages.scss";

function MainPage({ favorites, cart, toggleFavorite, isInCart, onClick }) {
    const [cards, setCards] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const results = await getData();
            setCards(results);
        };
        fetchData();
    }, []);

    return (
        <div className="main__page">
            <ProductList
                toggleFavorite={toggleFavorite}
                cards={cards}
                favorites={favorites}
                isInCart={isInCart}
                onClick={onClick}
                cart={cart}
            />
        </div>
    );
}

export default MainPage;
