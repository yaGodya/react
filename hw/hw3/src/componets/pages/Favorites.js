import React from "react";
import "./Pages.scss";
import ProductList from "../ProductsList";

function Favorites({ favorites, cart, toggleFavorite, isInCart, onClick }) {
    return (
        <div className="main__page">
            <ProductList
                toggleFavorite={toggleFavorite}
                cards={favorites}
                favorites={favorites}
                isInCart={isInCart}
                onClick={onClick}
                cart={cart}
            />
        </div>
    );
}

export default Favorites;
