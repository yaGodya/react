import React from "react";
import PropTypes from "prop-types";
import ProductCard from "./ProductCard";
import "../scss/ProductsList.scss";

function ProductList({
    isCartPage,
    cards,
    toggleFavorite,
    favorites,
    isInCart,
    onClick,
    cart,
}) {
    console.log(cart);
    return (
        <div className="main__wrapper">
            {cards.map((card) => (
                <ProductCard
                    onClick={onClick}
                    key={card.id}
                    card={card}
                    toggleFavorite={toggleFavorite}
                    isFavorite={favorites.some((p) => p.id === card.id)}
                    isInCart={
                        cart.find((item) => item.id === card.id) ? true : false
                    }
                    isCartPage={isCartPage}
                />
            ))}
        </div>
    );
}

export default ProductList;
