import React, { useState } from "react";
import PropTypes from "prop-types";
import Button from "./Buttons/Button";
import "../scss/ProductCard.scss";
import Modal from "./Modal/Modal";
import CardContent from "./pages/CardContent/CardContent";

function ProductCard({
    card,
    toggleFavorite,
    isInCart,
    onClick,
    isFavorite,
    isCartPage,
}) {
    const [isModalOpen, setModalOpen] = useState(false);

    const handleAddToCart = () => {
        setModalOpen(true);
    };

    const handleModalClose = () => {
        setModalOpen(false);
        onClick(card);
    };
    console.log(isInCart);
    const buttonText = isCartPage ? "Удалить из корзины" : "Добавить в корзину";
    const modalText = isCartPage
        ? "Будет удален из корзины"
        : "Будет добавлен в корзину";
    return (
        <li className="card-item">
            <CardContent
                card={card}
                toggleFavorite={toggleFavorite}
                isFavorite={isFavorite}
            />
            <Button
                onClick={handleAddToCart}
                text={buttonText}
                backgroundColor={"rgba(189, 189, 189, 0.596)"}
            />
            <Modal
                header="Подтверждение"
                closeButton={true}
                text={`Товар "${card.name} ${modalText}`}
                isOpen={isModalOpen}
                onRequestClose={() => setModalOpen(false)}
                actions={
                    <>
                        <Button
                            backgroundColor="rgba(189, 189, 189, 0.596)"
                            text="Добавить"
                            onClick={handleModalClose}
                        />
                        <Button
                            backgroundColor="rgba(189, 189, 189, 0.596)"
                            text="Отмена"
                            onClick={() => setModalOpen(false)}
                        />
                    </>
                }
            />
        </li>
    );
}

ProductCard.propTypes = {
    card: PropTypes.object.isRequired,
    toggleFavorite: PropTypes.func.isRequired,
    isFavorite: PropTypes.bool.isRequired,
};

export default ProductCard;
