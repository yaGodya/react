import React from "react";
import PropTypes from "prop-types";
import "./Modal.scss";

const Modal = ({
    header,
    closeButton,
    text,
    actions,
    isOpen,
    onRequestClose,
}) => {
    if (!isOpen) {
        return null;
    }

    return (
        <div className="modal-box" onClick={onRequestClose}>
            <div
                className="modal-box_content"
                onClick={(e) => e.stopPropagation()}
            >
                <div className="modal-box_header">
                    <h2 className="modal-box_title">{header}</h2>
                    {closeButton && (
                        <button
                            className="modal-box_close"
                            onClick={onRequestClose}
                        >
                            ×
                        </button>
                    )}
                </div>
                <div className="modal-box_text">{text}</div>
                <div className="modal-box_actions">{actions}</div>
            </div>
        </div>
    );
};

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired,
    actions: PropTypes.element.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onRequestClose: PropTypes.func.isRequired,
};

export default Modal;
