import PropTypes from "prop-types";
import "./Header.scss";
import { NavLink } from "react-router-dom";

function Header({ title, cart, favorites }) {
    return (
        <div className="header_wrapper">
            <div className="header">
                <NavLink to="/" className="header__title">
                    {title}
                </NavLink>
                <div className="header_btn-box">
                    <NavLink to="/favorites" className="header_btn">
                        Избранное
                        <span>
                            {favorites.length}
                            &#9734;
                        </span>
                    </NavLink>
                    <NavLink to="/cart" className="header_btn">
                        Корзина
                        <span>
                            {cart.length}
                            &#128722;
                        </span>
                    </NavLink>
                </div>
            </div>
        </div>
    );
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
    cart: PropTypes.arrayOf(PropTypes.object),
    favorites: PropTypes.arrayOf(PropTypes.object),
};

Header.defaultProps = {
    cart: [],
    favorites: [],
};

export default Header;
