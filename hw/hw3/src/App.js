import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import "./scss/App.scss";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./componets/pages/MainPage";
import Cart from "./componets/pages/Cart";
import Favorites from "./componets/pages/Favorites";
import Header from "./componets/Header/Header";
import Footer from "./componets/Footer/Footer";

function App() {
    const [isInCart, setIsInCart] = useState(false);
    const [cart, setCart] = useState([]);
    const [favorites, setFavorites] = useState([]);

    useEffect(() => {
        const savedFavorites = localStorage.getItem("favorites");
        if (savedFavorites) {
            setFavorites(JSON.parse(savedFavorites));
        }

        const savedCart = localStorage.getItem("cart");
        if (savedCart) {
            setCart(JSON.parse(savedCart));
        }
    }, []);

    useEffect(() => {
        localStorage.setItem("favorites", JSON.stringify(favorites));
    }, [favorites]);

    useEffect(() => {
        localStorage.setItem("cart", JSON.stringify(cart));
    }, [cart]);

    const addToFavorites = (product) => {
        setFavorites((prevFavorites) => [...prevFavorites, product]);
    };

    const removeFromFavorites = (card) => {
        setFavorites((prevFavorites) =>
            prevFavorites.filter((p) => p.id !== card.id)
        );
    };

    const toggleFavorite = (product) => {
        if (favorites.find((p) => p.id === product.id)) {
            removeFromFavorites(product);
        } else {
            addToFavorites(product);
        }
    };

    const addToCart = (product) => {
        setCart((prevCart) => [...prevCart, product]);
        setIsInCart(true);
    };

    const removeFromCart = (card) => {
        setCart((prevCart) => prevCart.filter((p) => p.id !== card.id));
        setIsInCart(false);
    };

    return (
        <BrowserRouter>
            <Header title={"P.Store"} cart={cart} favorites={favorites} />
            <Routes className="wrapper">
                <Route
                    path="/"
                    element={
                        <MainPage
                            isInCart={isInCart}
                            favorites={favorites}
                            cart={cart}
                            toggleFavorite={toggleFavorite}
                            onClick={addToCart}
                        />
                    }
                />

                <Route
                    path="/favorites"
                    element={
                        <Favorites
                            isInCart={isInCart}
                            favorites={favorites}
                            cart={cart}
                            toggleFavorite={toggleFavorite}
                            onClick={addToCart}
                        />
                    }
                />
                <Route
                    path="/cart"
                    element={
                        <Cart
                            isInCart={isInCart}
                            favorites={favorites}
                            cart={cart}
                            toggleFavorite={toggleFavorite}
                            onClick={removeFromCart}
                        />
                    }
                />
            </Routes>
            <Footer />
        </BrowserRouter>
    );
}

export default App;
