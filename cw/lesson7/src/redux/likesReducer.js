import { INCREMENT, DECREMENT } from "./type";

const initialState = {
    likes: 0,
};

export const likesReducer = (state = initialState, action) => {
    switch (action.type) {
        case INCREMENT:
            return {
                ...state,
                likes: state.likes + 1,
            };
        case DECREMENT:
            if (state.likes) {
                return {
                    ...state,
                    likes: state.likes - 1,
                };
            }
            break;
        default:
            return state;
    }
};
