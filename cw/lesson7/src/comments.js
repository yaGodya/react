import React, { useState, useEffect } from "react";
import SingleComment from "./singleComment.js";
import uniqid from "uniqid";
import { commentCreate, commentLoad } from "./redux/actions";
import { useDispatch, useSelector } from "react-redux";

function Comments(props) {
    const [textComment, setTextComment] = useState("");
    // console.log("props", props);
    const comments = useSelector((state) => {
        // console.log("redux state", state);
        const { commentsReducer } = state;
        return commentsReducer.comments;
    });
    const dispatch = useDispatch();
    const handleInput = (event) => {
        setTextComment(event.target.value);
    };

    useEffect(() => {
        dispatch(commentLoad());
    }, []);

    const handleSubmit = (event) => {
        event.preventDefault();
        // console.log("Submit", textComment);
        const id = uniqid();
        dispatch(commentCreate(textComment, id));
        setTextComment("");
    };
    console.log("comments", comments);
    return (
        <div className="card-comments">
            <form onSubmit={handleSubmit} className="comments-item-create">
                <input type="text" value={textComment} onChange={handleInput} />
                <input type="submit" hidden />
            </form>
            {!!comments.length &&
                comments.map((res) => {
                    return <SingleComment key={res.id} data={res} />;
                })}
        </div>
    );
}
export default Comments;
