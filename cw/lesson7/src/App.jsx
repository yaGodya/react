import './App.css'
import Likes from './Likes';
import Title from './redux/title';
import Comments from './comments';
import TailSpinner from './Spinner';

function App() {
  return (
    <div className="App">
      <div className="wrap">
        <TailSpinner/>
        <div className="card">
          <div className="card-image">
            <img src="./sea.jpg" alt="surfing"/>
            <Title/>
            <Likes/>
          </div>
          <Comments />
        </div>
      </div>
    </div>
  );
}

export default App;